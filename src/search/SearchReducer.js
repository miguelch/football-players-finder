import * as types from './SearchTypes';

const initialState = {
  foundPlayers: null,
  playerName: '',
  age: '',
  position: '',
};

export default function searchReducer(state = initialState, action) {
  switch (action.type) {
    case types.CLOSE_SNACKBAR:
        return {
          ...state,
          openSnackbar: false
        };
    case types.SEARCH_PLAYERS_INIT:
      return {
        ...state,
        isSearchingPlayers: true,
        errorSearching: false
      };
    case types.SEARCH_PLAYERS_COMPLETE:
      return {
        ...state,
        isSearchingPlayers: false,
        foundPlayers: action.payload
      };
    case types.SEARCH_PLAYERS_ERROR:
      return {
        ...state,
        isSearchingPlayers: false,
        foundPlayers: [],
        errorSearching: true,
        openSnackbar: true
      };
    case types.CHANGE_PLAYER_NAME:
      return {
        ...state,
        playerName: action.payload,
      };
    case types.CHANGE_POSITION:
      return {
        ...state,
        position: action.payload,
      };
    case types.CHANGE_AGE:
      return {
        ...state,
        age: action.payload,
      };
    default:
      return state;
  }
}
