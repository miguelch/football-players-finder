import { createSelector } from 'reselect';
import moment from 'moment';

export const positionSelector = state => state.search.position;
export const ageSelector = state => state.search.age;
export const playerNameSelector = state => state.search.playerName;
export const isSearchingPlayersSelector = state =>
  state.search.isSearchingPlayers;
export const foundPlayersSelector = state => state.search.foundPlayers;
export const openSnackbarSelector = state => state.search.openSnackbar;

export const playersFilteredByAge = createSelector(
  [ageSelector, foundPlayersSelector],
  (age, foundPlayers) => {
    if (Array.isArray(foundPlayers))
      return foundPlayers.filter(
        player =>
          !age ||
          moment().diff(moment(player.dateOfBirth, 'YYYY-MM-DD'), 'y') === age,
      );
    return null;
  },
);

export const playersFilteredByPosition = createSelector(
  [positionSelector, playersFilteredByAge],
  (position, foundPlayers) => {
    if (Array.isArray(foundPlayers))
      return foundPlayers.filter(
        player => !position || player.position === position,
      );
    return null;
  },
);

export const getFilteredPlayers = createSelector(
  [playerNameSelector, playersFilteredByPosition],
  (playerName, foundPlayers) => {
    if (Array.isArray(foundPlayers))
      return foundPlayers.filter(
        player =>
          !playerName ||
          player.name.toLowerCase().indexOf(playerName.toLowerCase()) !== -1,
      );
    return null;
  },
);
