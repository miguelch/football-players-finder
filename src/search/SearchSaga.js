import { put, call, takeLatest } from 'redux-saga/effects';
import * as types from './SearchTypes';


export function* searchPlayers() {
  try {
    const response = yield call(
      fetch,
      'https://football-players-b31f2.firebaseio.com/players.json?print=pretty',
    );
    const result = yield response.json();
    if(response.status===200&&Array.isArray(result)){
      yield put({
        type: types.SEARCH_PLAYERS_COMPLETE,
        payload: result
      })
    } else {
      yield put({
        type: types.SEARCH_PLAYERS_ERROR
      })
    }
  } catch (error) {
    yield put({
      type: types.SEARCH_PLAYERS_ERROR
    })
  }
}

export function* SearchSaga() {
  yield takeLatest(types.SEARCH_PLAYERS_INIT, searchPlayers);
}
