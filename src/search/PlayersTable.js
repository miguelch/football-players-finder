import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import moment from 'moment';

const useStyles = makeStyles({
  root: {
    width: '100%',
    overflowX: 'auto',
    minHeight: 300,
  },
  table: {
    minWidth: 650,
  },
  caption: {
    textAlign: 'center',
  },
  centerMessage: {
    display: 'flex',
    justifyContent: 'center',
  },
});

function getAge(dateOfBirth) {
  return moment().diff(moment(dateOfBirth, 'YYYY-MM-DD'), 'years');
}

function getMessage(players){
  if(!players)
    return 'Press the search button to search players'
  if(players&&players.length===0)
    return 'There is not players for the applied filters'
  return null
}

export default function PlayersTable({ players }) {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="players table">
        {!Array.isArray(players) || players.length === 0 ? (
          <caption className={`${classes.caption}`}>
            <Box className={classes.centerMessage}>
              <p>{getMessage(players)}</p>
            </Box>
          </caption>
        ) : null}
        <TableHead>
          <TableRow>
            <TableCell align="center">Player</TableCell>
            <TableCell align="center">Position</TableCell>
            <TableCell align="center">Nationality</TableCell>
            <TableCell align="center">Age</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Array.isArray(players) && players.length !== 0
            ? players.map(player => (
                <TableRow key={player.name}>
                  <TableCell component="th" scope="row" align="center">
                    {player.name}
                  </TableCell>
                  <TableCell align="center">{player.position}</TableCell>
                  <TableCell align="center">{player.nationality}</TableCell>
                  <TableCell align="center">
                    {getAge(player.dateOfBirth)}
                  </TableCell>
                </TableRow>
              ))
            : null}
        </TableBody>
      </Table>
    </Paper>
  );
}
