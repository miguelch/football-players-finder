import * as types from './SearchTypes';

export function changePlayerName(payload) {

  return {
    type: types.CHANGE_PLAYER_NAME,
    payload,
  };
}

export function changePosition(payload) {
  return {
    type: types.CHANGE_POSITION,
    payload,
  };
}

export function changeAge(payload) {
  return {
    type: types.CHANGE_AGE,
    payload,
  };
}

export function searchPlayers() {
  return {
    type: types.SEARCH_PLAYERS_INIT
  };
}

export function closeSnackbar() {
  return {
    type: types.CLOSE_SNACKBAR
  };
}

