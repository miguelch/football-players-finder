import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  container:{
    paddingBottom: 60
  },
  filtersContainer:{
    display: 'flex',
    justifyContent: 'space-between'
  },
  inputsContainer:{
    display: 'flex'
  },
  inputs: {
    marginRight: 20,
    minWidth: 200
  }
})

export default useStyles