import React from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Typography,
  Box,
  TextField,
  Button,
} from '@material-ui/core';
import AppToolbar from '../app/common/AppToolbar';
import useStyles from './styles';
import SelectField from '../app/common/SelectField';
import PlayersTable from './PlayersTable';
import {
  changePlayerName,
  changeAge,
  changePosition,
  searchPlayers,
  closeSnackbar,
} from './SearchActions';
import POSITIONS_TYPES from '../app/constants';
import {
  positionSelector,
  ageSelector,
  playerNameSelector,
  isSearchingPlayersSelector,
  getFilteredPlayers,
  openSnackbarSelector,
} from './SearchSelectors';
import AbsoluteLoader from '../app/common/AbsoluteLoader';
import MySnackbar from '../app/common/Snackbar';

function limitNumberKb(value, min, max) {
  let isValid = true
  if (value < min) isValid= false;
  if (value > max) isValid= false;
  return isValid
}

export const Search = ({
  onChangePlayerName,
  onChangePosition,
  onChangeAge,
  onCloseSnackbar,
  onSearch,
  position,
  age,
  isSearchingPlayers,
  foundPlayers,
  openSnackbar,
}) => {
  const classes = useStyles();
  return (
    <div>
      <AppToolbar title="Search" />
      <Container className={classes.container}>
        <br />
        <Typography variant="h4">Football player finder</Typography>
        <br />
        <Box className={classes.filtersContainer}>
          <Box className={classes.inputsContainer}>
            <TextField
              onChange={onChangePlayerName}
              className={classes.inputs}
              label="Player name"
            />
            <SelectField
              value={position}
              options={POSITIONS_TYPES}
              onChange={onChangePosition}
              label="Position"
              containerClass={classes.inputs}
            />
            <TextField
              value={age? age.toString(): ''}
              onChange={onChangeAge}
              label="Age"
              type="number"
              inputProps={{ min: 18, max: 40, maxLength: 2 }}
              className={classes.inputs}
            />
          </Box>
          <Button
            onClick={onSearch}
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Search
          </Button>
        </Box>
        <br />
        <br />
        <AbsoluteLoader visible={isSearchingPlayers}>
          <PlayersTable players={foundPlayers} />
        </AbsoluteLoader>
      </Container>
      <MySnackbar
        variant="error"
        message="There was an error, please check your network connection"
        open={openSnackbar}
        onClose={onCloseSnackbar}
      />
    </div>
  );
};

function mapStateToProps(state) {
  return {
    position: positionSelector(state),
    age: ageSelector(state),
    playerName: playerNameSelector(state),
    isSearchingPlayers: isSearchingPlayersSelector(state),
    foundPlayers: getFilteredPlayers(state),
    openSnackbar: openSnackbarSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onChangePlayerName: ({ target: { value } }) =>
      dispatch(changePlayerName(value)),
    onChangePosition: ({ target: { value } }) =>
      dispatch(changePosition(value)),
    onChangeAge: ({ target: { value } }) => {
      const age = parseInt(value, 10);
      if (value.length>1&&limitNumberKb(age, 18, 40)) dispatch(changeAge(age));
      if (value.length===0||value.length===1) dispatch(changeAge(null));
    },
    onSearch: () => dispatch(searchPlayers()),
    onCloseSnackbar: () => dispatch(closeSnackbar()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);
