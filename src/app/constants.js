const POSITIONS_TYPES = [
  {id: 'Keeper', name: 'Keeper'},
  {id: 'Centre-Back', name: 'Centre-Back'},
  {id: 'Left-Back', name: 'Left-Back'},
  {id: 'Right-Back', name: 'Right-Back'},
  {id: 'Defensive Midfield', name: 'Defensive-Midfield'},
  {id: 'Central Midfield', name: 'Central Midfield'},
  {id: 'Left Midfield', name: 'Left Midfield'},
  {id: 'Attacking Midfield', name: 'Attacking Midfield'},
  {id: 'Left Wing', name: 'Left Wing'},
  {id: 'Left-Forward', name: 'Left-Forward'},
  {id: 'Centre-Forward', name: 'Centre-Forward'},
  {id: 'Right-Forward', name: 'Right-Forward'},
]

export default POSITIONS_TYPES