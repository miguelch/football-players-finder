import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga'
import search from '../search/SearchReducer'
import { SearchSaga } from '../search/SearchSaga';
 // defaults to localStorage for web and AsyncStorage for react-native

const sagaMiddleware = createSagaMiddleware()

const reducers = combineReducers({
  search
});

export default function configureStore(){
  const store = createStore(
    reducers,
    undefined,
    compose(applyMiddleware(sagaMiddleware)),
  );
  sagaMiddleware.run(SearchSaga)
  return store
};
