import React from 'react';
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles(({ visible, overflow }) => ({
  container: {
    width: '100%',
    position: 'relative',
    maxHeight: visible ? '100vh' : 'none',
    overflow: visible && !overflow ? 'hidden' : overflow,
  },
}));


const AbsoluteLoader = ({visible, overflow, children, opacity, noLoader}) => {
  
  const classes = useStyles({visible, overflow})
  return (
    <Box
      className={classes.container}
    >
      {children}
      <div
        style={{
          // opacity: visible? 0.9 : 0.0,
          backgroundColor: visible
            ? `rgba(255,255,255, ${opacity||0.9})`
            : '#000088',
          transitionDuration: '2s',
          transitionProperty: 'opacity',
          display: visible ? 'flex' : 'none',
          alignItems: 'center',
          justifyContent: 'center',
          position: 'absolute',
          flexDirection: 'column',
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
        }}
      >
        {visible && !noLoader? (
          <CircularProgress tickness={3} size={30} color="primary" />
        ) : null}
      </div>
    </Box>
  );
};

AbsoluteLoader.propTypes = {
  visible: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  opacity: PropTypes.number,
  noLoader: PropTypes.bool,
  overflow: PropTypes.string,
};

export default AbsoluteLoader;
