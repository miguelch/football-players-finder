import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles({
  formControl: {
    minWidth: 200,
  },
});

export default function SelectField({
  label,
  onChange,
  value,
  containerClass,
  options,
}) {
  const classes = useStyles();
  return (
    <FormControl className={`${classes.formControl} ${containerClass}`}>
      <InputLabel htmlFor={label}>{label}</InputLabel>
      <Select
        value={value}
        onChange={onChange}
        inputProps={{
          name: label,
          id: label,
        }}
      >
        {Array.isArray(options)
          ? options.map(item => (
              <MenuItem key={item.id} value={item.id}>
                {item.name}
              </MenuItem>
            ))
          : null}
      </Select>
    </FormControl>
  );
}
